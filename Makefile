# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/02/18 21:15:16 by apyvovar          #+#    #+#              #
#    Updated: 2017/05/22 23:42:27 by apyvovar         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

#  COMMON
FLAGS = -Wall -Wextra -Werror
CC = gcc $(FLAGS)
LIBDIR = libft/
LIB = $(LIBDIR)libft.a
INC = includes
#  ASM
ASM = asm
ASM_DIR = src_asm/
ASM_FILES = asm.c op.c input.c head_mgmt.c oper_mgmt.c oper_tools.c oper_arg.c \
	tools.c output.c visualize.c clear.c
ASM_SRC = $(addprefix $(ASM_DIR), $(ASM_FILES))
ASM_OBJ = $(ASM_SRC:.c=.o)
#  VM
# VM = corewar
# VM_DIR = src_vm/
# VM_FILES = #here add .c files list
# VM_SRC = $(addprefix $(VM_DIR), $(VM_FILES))
# VM_OBJ = $(VM_SRC:.c=.o)

#  RUN
all: $(LIB) $(ASM) #$(VM)

$(LIB):
	make -C $(LIBDIR)

$(ASM): $(ASM_OBJ)
	$(CC) $(FLAGS) $(LIB) $(ASM_OBJ) -o $@

# $(VM): $(VM_OBJ)
	# $(CC) $(FLAGS) $(LIB) $(VM_OBJ) -o $@

.c.o: $(ASM_SRC) #$(VM_SRC)
	$(CC) $(FLAGS) -I $(INC) -I $(LIBDIR)$(INC) -c $^ -o $@

clean:
	make -C $(LIBDIR) clean
	/bin/rm -f $(ASM_OBJ)
	# /bin/rm -f $(VM_OBJ)

fclean: clean
	/bin/rm -f $(LIB) $(ASM) #$(VM)

re: fclean all
