/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   output.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/27 00:23:56 by apyvovar          #+#    #+#             */
/*   Updated: 2017/05/06 12:01:55 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

void	make_output(t_asm *champ)
{
	unsigned char	*file_body;
	t_command		*cmd;

	if (!(file_body = (unsigned char *)malloc(champ->head->prog_size)))
		return (clean_output(NULL, 0, 1, champ->filename));
	cmd = champ->commands;
	while (cmd)
	{
		if (!write_command(cmd, file_body))
			return (clean_output(file_body, 0, 0, NULL));
		cmd = cmd->next;
	}
	proceed_output(champ, file_body);
}

void	proceed_output(t_asm *champ, unsigned char *body)
{
	int		fd;

	ft_putstr(FMT_GREEN);
	ft_printf("Writing output program to %s\n", champ->filename);
	ft_putstr(FMT_OFF);
	if ((fd = open(champ->filename, OPEN_FLAGS, OPEN_MODE)) < 0)
		return (clean_output(NULL, fd, 1, champ->filename));
	if (write_head(champ, fd) < 0 || \
		write(fd, body, champ->head->prog_size) < 0)
		return (clean_output(body, fd, 1, champ->filename));
	clean_output(body, fd, 0, NULL);
}

int		write_head(t_asm *champ, int fd)
{
	unsigned int	nbr;
	int				i;

	nbr = champ->head->magic;
	nbr = (nbr & 0xff) << 24 | (nbr & 0xff00) << 8 | \
		(nbr & 0xff0000) >> 8 | (nbr & 0xff000000) >> 24;
	if (write(fd, &nbr, sizeof(nbr)) < 0 || \
		write(fd, champ->head->prog_name, sizeof(champ->head->prog_name)) < 0)
		return (-1);
	i = 0;
	while ((i++ + sizeof(champ->head->prog_name)) % sizeof(nbr))
		if (write(fd, "", 1) < 0)
			return (-1);
	nbr = champ->head->prog_size;
	nbr = (nbr & 0xff) << 24 | (nbr & 0xff00) << 8 | \
		(nbr & 0xff0000) >> 8 | (nbr & 0xff000000) >> 24;
	if (write(fd, &nbr, sizeof(nbr)) < 0 || \
		write(fd, champ->head->comment, sizeof(champ->head->comment)) < 0)
		return (-1);
	i = 0;
	while ((i++ + sizeof(champ->head->comment)) % sizeof(nbr))
		if (write(fd, "", 1) < 0)
			return (-1);
	return (1);
}

int		write_command(t_command *cmd, unsigned char *file)
{
	int		i;

	if (cmd->label && !cmd->oper_code)
		return (1);
	file += cmd->pos - 1;
	*file++ = (unsigned char)cmd->oper_code;
	if (cmd->code_byte)
		*file++ = cmd->code_byte;
	i = -1;
	while (++i < g_tab[cmd->oper_code - 1].arg_amount)
		file += write_arg(file, cmd->arg_size[i], cmd->intval[i]);
	return (1);
}

int		write_arg(unsigned char *file, size_t size, int intval)
{
	if (size == 1)
		*file = (unsigned char)intval;
	else if (size == 2)
	{
		intval = (unsigned short)intval;
		intval = intval >> 8 | (intval << 24) >> 16;
		*(unsigned short *)file = (unsigned short)intval;
	}
	else if (size == 4)
	{
		intval = (unsigned int)intval;
		intval = (intval & 0xff) << 24 | (intval & 0xff00) << 8 | \
			(intval & 0xff0000) >> 8 | (intval & 0xff000000) >> 24;
		*(unsigned int *)file = (unsigned int)intval;
	}
	else
		return (0);
	return (size);
}
