/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   input.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/21 12:51:15 by apyvovar          #+#    #+#             */
/*   Updated: 2017/05/15 19:45:03 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

void			input_parse(int fd, int anotate_flag, const char *file)
{
	t_asm	*champ;

	champ = init_champ(anotate_flag);
	if (!champ || !check_filename(file, champ) || !(champ->head = init_head()))
		return (clean_input(champ, fd));
	if (!parse_head(fd, champ, (char *)file))
		return (clean_input(champ, fd));
	if (!parse_commands(fd, champ, (char *)file))
		return (clean_input(champ, fd));
	if (champ->anotate_flag)
		make_visual(champ);
	else
		make_output(champ);
	clean_input(champ, fd);
}

int				check_filename(const char *filename, t_asm *champ)
{
	char	*temp;

	if (!(temp = ft_strrchr(filename, '.')) || ft_strcmp(temp, ".s"))
		return (err(ERR_FILE1, (char *)filename, 0));
	champ->filename = ft_strnew(temp - filename + 5);
	ft_strncpy(champ->filename, filename, temp - filename);
	ft_strcat(champ->filename, ".cor");
	return (1);
}

t_asm			*init_champ(int anotate_flag)
{
	t_asm			*champ;

	if (!(champ = (t_asm *)malloc(sizeof(t_asm))))
	{
		perr(NULL, 0);
		return (NULL);
	}
	champ->filename = NULL;
	champ->anotate_flag = anotate_flag;
	champ->commands = NULL;
	champ->head = NULL;
	return (champ);
}
