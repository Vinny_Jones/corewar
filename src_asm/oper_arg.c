/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   oper_arg.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/22 23:42:06 by apyvovar          #+#    #+#             */
/*   Updated: 2017/05/22 23:43:28 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

int			parse_command_arguments(t_command *cmd, char *line)
{
	char	*temp;
	int		i;

	if (!(cmd->arguments = ft_strsplit(line, SEPARATOR_CHAR)))
		return (perr(NULL, 0));
	i = -1;
	while ((temp = cmd->arguments[++i]))
	{
		cmd->arguments[i] = ft_strtrim(temp);
		free(temp);
		if (!cmd->arguments[i])
			return (perr(NULL, 0));
		if (i == g_tab[cmd->oper_code - 1].arg_amount)
			return (err(ERR_CMD3, cmd->arguments[i], cmd->line_num));
		if (!get_arg_type(cmd->arguments[i], i, cmd))
			return (0);
	}
	if (i != g_tab[cmd->oper_code - 1].arg_amount)
		return (err(ERR_CMD4, NULL, cmd->line_num));
	while (g_tab[cmd->oper_code - 1].codage && i++ < 3)
		cmd->code_byte <<= 2;
	cmd->size += (cmd->code_byte) ? 2 : 1;
	return (1);
}

int			get_arg_type(char *arg, int i, t_command *cmd)
{
	int		res;
	int		num;

	res = T_IND;
	if (*arg == 'r' || *arg == DIRECT_CHAR)
		res = (*arg++ == 'r') ? T_REG : T_DIR;
	if (res != T_REG && *arg == LABEL_CHAR)
	{
		while (*++arg && ft_strchr(LABEL_CHARS, *arg))
			;
		if (!*arg && ft_strchr(LABEL_CHARS, *--arg))
			return (parse_arg_type(cmd, i, res));
	}
	else
	{
		if (res == T_REG && ((num = ft_atoi(arg)) < 1 || num > REG_NUMBER))
			return (err(ERR_CMD5, arg, cmd->line_num));
		arg = (*arg == '-') ? ++arg : arg;
		while (*arg && ft_isdigit(*arg))
			arg++;
		if (!*arg && ft_isdigit(*--arg))
			return (parse_arg_type(cmd, i, res));
	}
	return (err(ERR_CMD5, arg, cmd->line_num));
}

int			parse_arg_type(t_command *cmd, int i, int type)
{
	int		arg_byte;
	int		byte_codage;
	int		dir_len;

	arg_byte = g_tab[cmd->oper_code - 1].args[i];
	if ((type | arg_byte) != arg_byte)
		return (err(ERR_CMD6, cmd->arguments[i], cmd->line_num));
	byte_codage = g_tab[cmd->oper_code - 1].codage;
	if (byte_codage && type == T_REG)
		cmd->code_byte = (cmd->code_byte | 0b01) << 2;
	else if (byte_codage && type == T_DIR)
		cmd->code_byte = (cmd->code_byte | 0b10) << 2;
	else if (byte_codage && type == T_IND)
		cmd->code_byte = (cmd->code_byte | 0b11) << 2;
	dir_len = (g_tab[cmd->oper_code - 1].d_len) ? 2 : 4;
	if (type == T_REG)
		cmd->arg_size[i] = 1;
	else
		cmd->arg_size[i] = (type == T_DIR ? dir_len : 2);
	cmd->size += cmd->arg_size[i];
	return (1);
}
