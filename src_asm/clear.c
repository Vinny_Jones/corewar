/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   clear.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/23 18:29:40 by apyvovar          #+#    #+#             */
/*   Updated: 2017/05/06 15:15:50 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

void	clean_output(unsigned char *file, int fd, int errflag, char *errmsg)
{
	if (fd > 0)
		close(fd);
	if (file)
		free(file);
	if (errflag)
		perr(errmsg, 0);
}

void	clean_input(t_asm *champ, int fd)
{
	if (fd > 0)
		close(fd);
	if (!champ)
		return ;
	if (champ->head)
		free(champ->head);
	if (champ->filename)
		free(champ->filename);
	clean_commands(champ->commands);
	free(champ);
}

void	clean_commands(t_command *start)
{
	if (!start)
		return ;
	clean_commands(start->next);
	if (start->label)
		free(start->label);
	clean_arguments(start->arguments);
	free(start);
}

void	clean_arguments(char **args)
{
	int		i;

	if (!args)
		return ;
	i = -1;
	while (args[++i])
		free(args[i]);
	free(args);
}
