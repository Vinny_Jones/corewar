/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   head_mgmt.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/23 16:06:20 by apyvovar          #+#    #+#             */
/*   Updated: 2017/05/11 17:55:00 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

t_header	*init_head(void)
{
	t_header	*head;
	int			i;

	if (!(head = (t_header *)malloc(sizeof(t_header))))
	{
		perr(NULL, 0);
		return (NULL);
	}
	head->magic = COREWAR_EXEC_MAGIC;
	head->prog_size = 0;
	i = 0;
	while (i < PROG_NAME_LENGTH)
		head->prog_name[i++] = '\0';
	i = 0;
	while (i < COMMENT_LENGTH)
		head->comment[i++] = '\0';
	return (head);
}

int			parse_head(int fd, t_asm *champ, char *file)
{
	ssize_t	l1;
	ssize_t	l2;
	char	*line;

	line = NULL;
	l1 = get_command_line(fd, &line);
	if (!(l1 = check_head(l1, line, champ, file)))
		return (0);
	l2 = get_command_line(fd, &line);
	if (!(l2 = check_head(l2, line, champ, file)))
		return (0);
	if (l1 != l2)
		return (1);
	return (err((l1 == 1) ? ERR_HEAD9 : ERR_HEAD10, NULL, 0));
}

int			check_head(ssize_t line_num, char *line, t_asm *champ, char *file)
{
	if (line_num < 0)
		return (perr(file, 0));
	if (!line_num)
		return (err(ERR_HEAD11, file, 0));
	if (!ft_strncmp(NAME_CMD_STRING, line, ft_strlen(NAME_CMD_STRING)))
		return (head_save(champ, line, line_num, 1));
	if (!ft_strncmp(COMMENT_CMD_STRING, line, ft_strlen(COMMENT_CMD_STRING)))
		return (head_save(champ, line, line_num, 2));
	if (!*champ->head->prog_name && !*champ->head->comment)
		err(ERR_HEAD1, line, line_num);
	else if (*champ->head->prog_name)
		err(ERR_HEAD2, line, line_num);
	else
		err(ERR_HEAD3, line, line_num);
	free(line);
	return (0);
}

int			head_save(t_asm *champ, char *line, ssize_t line_num, int flag)
{
	int		errflag;
	int		i;
	char	*temp;
	char	*errmsg;

	temp = line;
	line += ft_strlen((flag == 1) ? NAME_CMD_STRING : COMMENT_CMD_STRING);
	while (ft_isspace(*line))
		line++;
	errflag = (*line++ == '\"') ? 0 : 1;
	i = 0;
	while (line[i] && line[i] != '\"')
		i++;
	errflag = (!line[i] || line[i + 1]) ? 1 : 0;
	errmsg = get_head_errmsg(flag, errflag);
	if (!errflag && flag == 1 && i <= PROG_NAME_LENGTH)
		ft_strncpy(champ->head->prog_name, line, i);
	else if (!errflag && flag == 2 && i <= COMMENT_LENGTH)
		ft_strncpy(champ->head->comment, line, i);
	else if (!errflag)
		errmsg = (flag == 1) ? ERR_HEAD6 : ERR_HEAD7;
	errmsg = (flag == 1 && !i) ? ERR_HEAD8 : errmsg;
	free(temp);
	return ((errmsg) ? err(errmsg, NULL, line_num) : flag);
}

char		*get_head_errmsg(int flag, int errflag)
{
	if (errflag)
		return (flag == 1 ? ERR_HEAD4 : ERR_HEAD5);
	return (NULL);
}
