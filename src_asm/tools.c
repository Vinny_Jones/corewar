/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   tools.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/03 15:03:30 by apyvovar          #+#    #+#             */
/*   Updated: 2017/05/11 16:37:52 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

ssize_t	get_command_line(int fd, char **line)
{
	static size_t	line_num;
	int				gnl_flag;
	char			*temp;

	while ((gnl_flag = get_next_line(fd, &temp)) > 0)
	{
		line_num++;
		*line = ft_strtrim(remove_comment(temp));
		free(temp);
		if (**line && **line != COMMENT_CHAR)
			return (line_num);
		free(*line);
		*line = NULL;
	}
	return (gnl_flag);
}

char	*remove_comment(char *line)
{
	char	*start;

	start = line;
	while (*line && *line != COMMENT_CHAR && *line != ';')
		line++;
	while (*line)
		*line++ = '\0';
	return (start);
}

int		err(char *errmsg, char *target, size_t line_num)
{
	ft_putstr(FMT_RED);
	ft_putstr("Error");
	if (target)
		ft_printf(" at [%s]", target);
	if (line_num)
		ft_printf(" in line %zu", line_num);
	ft_putstr(FMT_OFF);
	ft_putstr(": ");
	if (errmsg)
		ft_printf("%s\n", errmsg);
	else
		perror("");
	return (0);
}

int		perr(char *target, size_t line_num)
{
	return (err(NULL, target, line_num));
}
