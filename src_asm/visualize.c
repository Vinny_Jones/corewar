/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   visualize.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/05 12:46:47 by apyvovar          #+#    #+#             */
/*   Updated: 2017/05/11 16:38:20 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

void	make_visual(t_asm *champ)
{
	t_command	*cmd;

	fmt_print("Dumping annotated program on standard output\n", 0, 3, 0);
	ft_printf("Program size : %u bytes\n", champ->head->prog_size);
	ft_putstr("Name: \"");
	fmt_print(champ->head->prog_name, 10, 7, 0);
	ft_putstr("\"\n\nPos. (Size):  [Label:] Op.(CB)  Arg. 1 (int)");
	ft_putstr("    Arg. 2 (int)    Arg. 3 (int)\n");
	cmd = champ->commands;
	while (cmd)
	{
		if (cmd->label)
			ft_printf("%-11zu   %s:\n", cmd->pos - 1, cmd->label);
		if (cmd->oper_code)
		{
			ft_printf("%-4zu (%-4zu):           ", cmd->pos - 1, cmd->size);
			ft_printf("%-5s    ", g_tab[cmd->oper_code - 1].oper);
			visual_arguments(cmd);
			visual_arg_dump(cmd);
		}
		cmd = cmd->next;
	}
}

void	visual_arguments(t_command *cmd)
{
	int		i;
	int		chars;

	i = -1;
	while (++i < g_tab[cmd->oper_code - 1].arg_amount)
	{
		chars = ft_printf("%s ", cmd->arguments[i]);
		chars += ft_printf("(%i)", cmd->intval[i]);
		while (chars++ < 16)
			ft_putchar(' ');
	}
	ft_putchar('\n');
}

void	visual_arg_dump(t_command *cmd)
{
	int		i;

	ft_printf("                       %.2X", cmd->oper_code);
	if (cmd->code_byte)
		ft_printf("(%.2X)", cmd->code_byte);
	else
		ft_putstr("    ");
	ft_putstr("   ");
	i = -1;
	while (++i < g_tab[cmd->oper_code - 1].arg_amount)
		visual_arg(cmd->arg_size[i], cmd->intval[i]);
	ft_putstr("\n\n");
}

void	visual_arg(size_t size, int intval)
{
	int		i;

	if (size == 1)
		ft_printf("%.2X  ", (unsigned char)intval);
	else if (size == 2)
	{
		ft_printf("%.2X  ", (unsigned char)(intval >> 8));
		ft_printf("%.2X  ", (unsigned char)intval);
	}
	else if (size == 4)
	{
		ft_printf("%.2X  ", (unsigned char)(intval >> 24));
		ft_printf("%.2X  ", (unsigned char)(intval >> 16));
		ft_printf("%.2X  ", (unsigned char)(intval >> 8));
		ft_printf("%.2X  ", (unsigned char)intval);
	}
	i = 0;
	while (i++ + size * 4 < 16)
		ft_putchar(' ');
}
