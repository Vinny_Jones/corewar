/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   oper_tools.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/05 18:54:38 by apyvovar          #+#    #+#             */
/*   Updated: 2017/05/06 12:20:33 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

int			set_arg_intval(t_command *start)
{
	int			i;
	char		*arg;
	t_command	*cmd;

	cmd = start;
	while (cmd)
	{
		i = -1;
		while (cmd->oper_code && ++i < g_tab[cmd->oper_code - 1].arg_amount)
		{
			arg = cmd->arguments[i];
			arg = (*arg == DIRECT_CHAR || *arg == 'r') ? ++arg : arg;
			if (*arg != LABEL_CHAR)
				cmd->intval[i] = ft_atoi(arg);
			else if ((cmd->intval[i] = label_intval(start, cmd, ++arg)) == -1)
				return (err(ERR_CMD7, cmd->arguments[i], cmd->line_num));
		}
		cmd = cmd->next;
	}
	return (1);
}

int			label_intval(t_command *start, t_command *current, char *arg)
{
	while (start)
	{
		if (start->label && !ft_strcmp(start->label, arg))
			return (start->pos - current->pos);
		start = start->next;
	}
	return (-1);
}
