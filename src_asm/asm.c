/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   asm.c                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/21 11:29:12 by apyvovar          #+#    #+#             */
/*   Updated: 2017/05/11 16:26:46 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

int		main(int argc, const char *argv[])
{
	int		i;
	int		anotate_flag;
	int		*f_desc;

	if (argc > 1)
	{
		anotate_flag = 0;
		i = 0;
		while (++i < argc)
			if (!ft_strcmp(argv[i], "-a"))
				anotate_flag = 1;
		if (!(f_desc = open_all(argc, argv)))
			return (0);
		i = -1;
		while (++i < argc - 1)
			if (f_desc[i] > 0)
				input_parse(f_desc[i], anotate_flag, argv[i + 1]);
	}
	else
		ft_putstr("Usage: ./asm [-a] src_1.s ... src_N.s\n    -a : Instead \
of .cor file, outputs a stripped and annotated version of code\n");
	return (0);
}

int		*open_all(int argc, const char *argv[])
{
	int		i;
	int		*result;

	if (!(result = (int *)malloc(sizeof(int) * argc)))
	{
		perr(NULL, 0);
		return (NULL);
	}
	i = 0;
	while (++i < argc)
		result[i - 1] = (ft_strcmp(argv[i], "-a")) ? get_fd(argv[i]) : 0;
	return (result);
}

int		get_fd(const char *file)
{
	int		fd;

	if ((fd = open(file, O_RDONLY)) > 0)
		return (fd);
	return (perr((char *)file, 0));
}
