/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   oper_mgmt.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/29 00:06:09 by apyvovar          #+#    #+#             */
/*   Updated: 2017/05/22 23:58:52 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "asm.h"

int			parse_commands(int fd, t_asm *champ, char *file)
{
	char		*line;
	ssize_t		line_num;
	t_command	*last_cmd;

	last_cmd = NULL;
	line = NULL;
	while ((line_num = get_command_line(fd, &line)) > 0)
	{
		if (!(save_command(&last_cmd, &line, line_num)))
		{
			free(line);
			return (0);
		}
		if (!champ->commands)
			champ->commands = last_cmd;
		champ->head->prog_size += last_cmd->size;
	}
	if (line_num < 0)
		return (perr(file, 0));
	if (!champ->commands)
		return (err(ERR_CMD1, file, 0));
	return (set_arg_intval(champ->commands));
}

size_t		save_command(t_command **cmd, char **line, size_t line_num)
{
	size_t		pos;
	t_command	*new;

	pos = (*cmd) ? ((*cmd)->pos + (*cmd)->size) : 1;
	if (!(new = cmd_init(cmd)))
		return (perr(NULL, 0));
	new->line_num = line_num;
	new->size = 0;
	new->pos = pos;
	new->next = NULL;
	new->code_byte = 0;
	new->label = get_command_label(*line);
	new->oper_code = 0;
	new->arguments = NULL;
	new->arg_size[0] = 0;
	new->arg_size[1] = 0;
	new->arg_size[2] = 0;
	if (!command_oper_mgmt(new, *line))
		return (0);
	free(*line);
	*line = NULL;
	return (1);
}

t_command	*cmd_init(t_command **cmd)
{
	if (!*cmd)
		*cmd = (t_command *)malloc(sizeof(t_command));
	else
	{
		(*cmd)->next = (t_command *)malloc(sizeof(t_command));
		*cmd = (*cmd)->next;
	}
	return (*cmd);
}

int			command_oper_mgmt(t_command *cmd, char *line)
{
	char	*temp;
	int		i;
	int		len;

	line += (cmd->label ? ft_strlen(cmd->label) + 1 : 0);
	while (ft_isspace(*line))
		line++;
	if (!*line)
		return (1);
	i = -1;
	while (*(temp = g_tab[++i].oper))
	{
		len = ft_strlen(temp);
		if (ft_isspace(line[len]) && !ft_strncmp(line, temp, len))
		{
			cmd->oper_code = g_tab[i].oper_code;
			line += len;
			return (parse_command_arguments(cmd, line));
		}
	}
	i = 0;
	while (line[i] && !ft_isspace(line[i]))
		i++;
	line[i] = '\0';
	return (err(ERR_CMD2, line, cmd->line_num));
}

char		*get_command_label(char *line)
{
	char	*temp;

	temp = line;
	while (*temp && ft_strchr(LABEL_CHARS, *temp))
		temp++;
	if (!*temp || *temp != LABEL_CHAR || !(temp - line))
		return (NULL);
	return (ft_strsub(line, 0, temp - line));
}
