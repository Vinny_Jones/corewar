/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printf.h                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/01/03 16:37:50 by apyvovar          #+#    #+#             */
/*   Updated: 2017/01/24 17:53:25 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTF_H
# define FT_PRINTF_H
# include <stdarg.h>
# include <stdint.h>
# include <wchar.h>
# define CONV "sSpdDioOuUxXcCfFn%"
# define SKIP "#\'+ -0123456789hlLjz.,;_*"
# define SET_AP(X) (X) ? ap_first : ap
# define CFMT(C) (ft_strchr(fmt, C))
# define ACFMT(C) (CFMT(ft_toupper(C)) || CFMT(ft_tolower(C)))
# define INTTYPE_I(N) (int)N
# define INTTYPE_H(N) (short int)N
# define INTTYPE_HH(N) (signed char)N
# define INTTYPE_L(N) (long int)N
# define INTTYPE_LL(N) (long long int)N
# define INTTYPE_Z(N) (size_t)N
# define UINTTYPE_I(N) (unsigned int)N
# define UINTTYPE_H(N) (unsigned short int)N
# define UINTTYPE_HH(N) (unsigned char)N
# define UINTTYPE_L(N) (unsigned long int)N
# define UINTTYPE_LL(N) (unsigned long long int)N

int			ft_printf(const char *format, ...);
int			print_arg(va_list ap_first, va_list ap, char *fmt, int len);
int			print_unknown(char *fmt, int len);
void		skip_arg(va_list ap, char *fmt);

int			print_int(va_list ap, char *fmt, int width, int prec);
char		*save_int(va_list ap, char *fmt, int num_arg);
char		*int_itoa(intmax_t n);
intmax_t	getnumarg(va_list ap, int n, char *fmt);
int			getnextarg_int(va_list ap, int len, char *fmt);

int			print_double(va_list ap, char *fmt, int width, int prec);
char		*save_double(va_list ap, char *fmt, int num_arg, int prec);
long double	getnumarg_double(va_list ap, int n, int l_flag);
char		*ft_ftoa(char *fmt, long double n, int prec);
void		save_dpart(char *result, unsigned long long int n, int prec);

int			print_unsigned(va_list ap, char *fmt, int width, int prec);
char		*save_uint(va_list ap, char *fmt, int num_arg);
char		*uint_itoa_base(uintmax_t n, char *fmt, unsigned int base);
uintmax_t	getnumarg_uint(va_list ap, int n);

int			print_char(va_list ap, char *fmt, int num_arg, int width);
int			get_char(va_list ap, char *fmt, int num_arg);
int			write_wchar(int	ch, char *fmt, int width);
int			save_wchar(unsigned char *wch, int ch);
int			inttooct(int nb);
int			octtoint(int nb);

int			print_str(va_list ap, char *fmt, int width, int prec);
int			print_s(char *str, int width, int prec, char *fmt);
char		*get_str(va_list ap, int num_arg, char *fmt);
int			print_wstr(wchar_t *str, int width, int prec, char *fmt);
int			wstr_len(wchar_t *str, int prec);
wchar_t		*get_wstr(va_list ap, int num_arg, char *fmt);

void		print_nspec(va_list ap_f, va_list ap, char *fmt, int count);
int			*getnumptr(va_list ap, int n);

int			getwidth(va_list ap_first, va_list ap, char *fmt, int len);
int			parse_width(va_list ap_f, va_list ap, char *fmt, int len);
int			getprecision(va_list ap_first, va_list ap, char *fmt, int len);
void		apply_flprec_int(int prec, char *fmt, char **str, int len);
void		apply_flwid_int(int width, char *fmt, char **result, int prec);

int			fmt_len(char *fmt);
int			fmt_isnumarg(char *fmt);
void		fmt_fin(va_list ap_f, va_list ap, char *fmt, int count);

void		apply_thousand_sep(char **result, int len);
void		apply_sign_flag(char *fmt, char **str);
void		apply_hash_flag(char **result, char *fmt, int len);
void		apply_nul_flag(char **result, char *fmt, int prec);
int			isnulflag(char *fmt);

#endif
