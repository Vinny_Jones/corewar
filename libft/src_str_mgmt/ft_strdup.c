/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strdup.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/11/23 12:43:01 by apyvovar          #+#    #+#             */
/*   Updated: 2017/02/18 17:31:05 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_strdup(const char *src)
{
	char	*res;
	size_t	i;

	res = (char *)malloc(sizeof(char) * ft_strlen(src) + 1);
	if (!res)
		return (NULL);
	i = -1;
	while (src[++i])
		res[i] = src[i];
	res[i] = '\0';
	return (res);
}
