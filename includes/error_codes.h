/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   error_codes.h                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/05/03 00:35:26 by apyvovar          #+#    #+#             */
/*   Updated: 2017/05/11 16:23:27 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ERROR_CODES_H
# define ERROR_CODES_H

# define ERR_FILE1 "Wrong input file type - <sourcefile.s> required"

# define ERR_HEAD1 "Line must contain champion name or description"
# define ERR_HEAD2 "Line must contain description"
# define ERR_HEAD3 "Line must include champion name"
# define ERR_HEAD4 "Champion name must be in double brackets (\"value\")"
# define ERR_HEAD5 "Champion comment must be in double brackets (\"value\")"
# define ERR_HEAD6 "Champion name too long"
# define ERR_HEAD7 "Champion comment too long"
# define ERR_HEAD8 "Champion name can\'t be empty"
# define ERR_HEAD9 "Multiple champion name line"
# define ERR_HEAD10 "Multiple champion comment line"
# define ERR_HEAD11 "Incomplete input data"

# define ERR_CMD1 "Champion must contain at least one instruction"
# define ERR_CMD2 "Invalid instruction"
# define ERR_CMD3 "Too many arguments for this instruction type"
# define ERR_CMD4 "Not enough arguments for this instruction type"
# define ERR_CMD5 "Invalid argument (check syntax)"
# define ERR_CMD6 "Invalid argument type for this operation"
# define ERR_CMD7 "No such label"

#endif
