/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   asm.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: apyvovar <apyvovar@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/04/21 11:27:09 by apyvovar          #+#    #+#             */
/*   Updated: 2017/05/23 00:04:50 by apyvovar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef ASM_H
# define ASM_H
# include <fcntl.h>
# include <stdlib.h>
# include <stdio.h>
# include "op.h"
# include "error_codes.h"
# include "libft.h"
# define OPEN_FLAGS O_RDWR|O_TRUNC|O_CREAT
# define OPEN_MODE S_IREAD|S_IWUSR

typedef struct	s_op
{
	char	oper[10];
	int		arg_amount;
	int		args[3];
	int		oper_code;
	int		cycles;
	char	oper_name[50];
	int		codage;
	int		d_len;
}				t_op;

typedef struct	s_command
{
	struct s_command	*next;
	size_t				line_num;
	size_t				size;
	size_t				pos;
	unsigned char		code_byte;
	char				*label;
	unsigned int		oper_code;
	char				**arguments;
	int					intval[3];
	int					arg_size[3];
}				t_command;

typedef struct	s_asm
{
	char		*filename;
	int			anotate_flag;
	t_header	*head;
	t_command	*commands;
}				t_asm;

extern t_op		g_tab[];

int				*open_all(int argc, const char *argv[]);
int				get_fd(const char *file);

void			input_parse(int fd, int anotate_flag, const char *file);
int				check_filename(const char *filename, t_asm *champ);
t_asm			*init_champ(int anotate_flag);

t_header		*init_head(void);
int				parse_head(int fd, t_asm *champ, char *file);
int				check_head(ssize_t line_n, char *line, t_asm *champ, char *f);
int				head_save(t_asm *champ, char *line, ssize_t line_num, int flag);
char			*get_head_errmsg(int flag, int errflag);

int				parse_commands(int fd, t_asm *champ, char *file);
size_t			save_command(t_command **last, char **line, size_t line_num);
t_command		*cmd_init(t_command **cmd);
int				command_oper_mgmt(t_command *last, char *line);
char			*get_command_label(char *line);

int				parse_command_arguments(t_command *last, char *line);
int				get_arg_type(char *arg, int i, t_command *cmd);
int				parse_arg_type(t_command *cmd, int i, int type);

int				set_arg_intval(t_command *start);
int				label_intval(t_command *start, t_command *current, char *arg);

void			make_output(t_asm *champ);
void			proceed_output(t_asm *champ, unsigned char *body);
int				write_command(t_command *cmd, unsigned char *file);
int				write_arg(unsigned char *file, size_t size, int intval);
int				write_head(t_asm *champ, int fd);

void			make_visual(t_asm *champ);
void			visual_arguments(t_command *cmd);
void			visual_arg_dump(t_command *cmd);
void			visual_arg(size_t size, int intval);

ssize_t			get_command_line(int fd, char **line);
char			*remove_comment(char *line);
int				err(char *errmsg, char *target, size_t line_num);
int				perr(char *target, size_t line_num);

void			clean_output(unsigned char *f, int fd, int ef, char *em);
void			clean_input(t_asm *champ, int fd);
void			clean_commands(t_command *start);
void			clean_arguments(char **args);

#endif
